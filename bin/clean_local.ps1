## THIS UTILITY MOVES ALL .conf FILES FROM %SPLUNK_HOME%\etc\system\local
## TO %SPLUNK_HOME%\etc\apps\zz-migrated-system-local\local TO INSURE 
## THAT THESE SETTINGS ARE PRESERVED, BUT MADE THE SETTINGS OF LAST RESORT
## TO ALLOW THE DEPLOYMENT-APPS TO CONTROL THE CONFIGURATION OF THE INSTANCE.

## CURRENT CONTENTS OF THE .conf FILES ARE APPENDED TO THE FILE OF THE SAME
## NAME IN %SPLUNK_HOME%\etc\apps\zz-migrated-system-local\local AND A LITERAL
## COPY IS ALSO SAVED WITH THE CURRENT TIMESTAMP APPENDED. DUE TO THE BEHAVIOR
## OF SPLUNK, ONLY THE LAST SETTING IN THE FILE IS HONORED, SO BY APPENDING
## WE INSURE THAT THE LATEST VALUES FROM %SPLUNK_HOME%\etc\system\local ARE
## STILL EFFECTIVE, HOWEVER NOW AT A LOWER PRIORITY.

## IF THE FILES ARE NOT NEEDED THEY CAN SAFELY BE DELETED. 

## THESE CHANGES, HOWEVER, WILL NOT TAKE EFFECT UNTIL SPLUNK IS RESTARTED.

if (!(Test-Path $env:SPLUNK_HOME)) {
	echo "SPLUNK_HOME not set -- Exiting. This script is designed to run as a scripted input."
	Get-ChildItem env:
	exit 1
} else {
	$SPLUNK_HOME="$env:SPLUNK_HOME"
	$SPLUNK_ETC="$SPLUNK_HOME\etc"
}

$scriptName = $myInvocation.MyCommand.Path
$timeStamp = (Get-Date -UFormat "+%Y%m%dT%H%M%S")
$destPath = "$SPLUNK_ETC\apps\zz-migrated-system-local\local"
$sourcePath = "$SPLUNK_ETC\system\local"
$counter = "0"

$header = "`r`n## $timeStamp`r`n## Migrated from $sourcePath`r`n"

$header="## THIS SPLUNK INSTANCE IS MANANGED BY A DEPLOYMENT SERVER`r`n`r`n"
$header="$header## CURRENT CONTENTS OF THIS .conf FILE HAS BEEN APPENDED TO THE FILE OF THE SAME`r`n"
$header="$header## NAME IN `$SPLUNK_HOME/etc/apps/zz-migrated-system-local/local AND A LITERAL`r`n"
$header="$header## COPY IS ALSO SAVED WITH THE CURRENT TIMESTAMP APPENDED. DUE TO THE BEHAVIOR`r`n"
$header="$header## OF SPLUNK, ONLY THE LAST SETTING IN THE FILE IS HONORED, SO BY APPENDING`r`n"
$header="$header## WE INSURE THAT THE LATEST VALUES FROM `$SPLUNK_HOME/etc/system/local ARE`r`n"
$header="$header## STILL EFFECTIVE, HOWEVER NOW AT A LOWER PRIORITY.`r`n`r`n"
$header="$header## IF THE FILES ARE NOT NEEDED THEY CAN SAFELY BE DELETED.`r`n`r`n" 
$header="$header## THESE CHANGES, HOWEVER, WILL NOT TAKE EFFECT UNTIL SPLUNK IS RESTARTED.`r`n`r`n"

$message2="## TO $destPath`r`n## TO INSURE DEPLOYED APPS TAKE PRECEDENCE`r`n"

$footer="`r`n## END MIGRATED SETTINGS`r`n"


foreach ($sourceFile in (Get-ChildItem -Path "$sourcePath\*.conf" -Exclude "serverclass.conf" ).Name) {

	$message1="`r`n## $timeStamp`r`n## SETTINGS MIGRATED BY $scriptName`r`n## FROM $sourcePath\$sourceFile`r`n"
	
	if (Test-Path -Path "$sourcePath\$sourceFile" -PathType Leaf)
	{
	    if (!(Test-Path -Path $destPath -PathType Container))
	    {
	        New-Item -ItemType Directory -Path $destPath | Out-Null
	    }
	
	    Add-Content -Path "$destPath\$sourceFile" -Value("$message1")
	    Add-Content -Path "$destPath\$sourceFile" -Value(Get-Content "$sourcePath\$sourceFile")
	    Add-Content -Path "$destPath\$sourceFile" -Value("$footer")
	
	    Move-Item -Path "$sourcePath\$sourceFile" -Destination "$destPath\$sourceFile.$timeStamp"
	    Add-Content -Path "$sourcePath\$sourceFile.moved" -Value("$header$message1$message2")
	}

}
